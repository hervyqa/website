---
layout: blog
draft: false
date: 2023-05-30T19:09:28+07:00
title: "Cara Memasang R dan RStudio Di NixOS"
description: "Konfigurasi bahasa pemrograman R dan RStudio IDE untuk statistika dan analisis data di NixOS."
image: "images/blog/cara-memasang-r-dan-rstudio-di-nixos.png"
type: "regular" # featured/regular
categories: # max 2
- rstudio
- nixos
---

Bahasa pemrograman R lebih dikenal sebagai bahasa pemrograman untuk
statistika dan visualisasi grafis. Dibuat oleh
[Ross Ihaka](https://en.wikipedia.org/wiki/Ross_Ihaka) dan
[Robert Gentleman](https://en.wikipedia.org/wiki/Robert_Gentleman_(statistician))
di Universitas Auckland, dan sekarang dikembangkan oleh R Development Core Team.

- Website: [r-project.org](https://r-project.org)
- Dokumentasi: [r-project.org/other-docs](https://r-project.org/other-docs)
- Buku: [rstudio.com/resources/books](https://rstudio.com/resources/books)

Bahasa R dibawah lisensi GNU GPL telah menjadi standar de facto di antara
statistikawan untuk pengembangan perangkat lunak statistika, serta digunakan
secara luas untuk pengembangan perangkat lunak statistika dan analisis data.

Penggunaan bahasa R dapat dilakukan dengan [Jupyter Notebook](https://jupyter.org)
atau [RStudio](https://r-project.org.org) untuk memudahkan data scientist untuk
mengolah data.

Seorang data analyst juga diharuskan untuk memiliki keterampilan teknis bahasa
pemrograman R. IDE yang sesuai dengan bahasa R adalah RStudio Desktop.

{{< image src="images/blog/rstudio-ide.png" alt="RStudio IDE for R" >}}

- Website: [posit.co/products/open-source/rstudio](https://posit.co/products/open-source/rstudio)
- Dokumentasi: [docs.posit.co/ide/user/ide/get-started](https://docs.posit.co/ide/user/ide/get-started)
- Kode sumber: [github.com/rstudio/rstudio](https://github.com/rstudio/rstudio)

Uniknya RStudio tidak hanya untuk bahasa R saja, tetapi terintegrasi dengan
bahasa pemrograman Python juga. Fitur RStudio sudah termasuk console, syntax
highlight, alat untuk plotting, debugging dan manajemen area kerja (workspace).

RStudio tersedia versi open source edisi komunitas dan edisi komersil yang
keduanya dapat berjalan di sistem operasi Windows, Mac, dan Linux.

{{< lineads_1 >}}

#### Pemasangan di NixOS

Mengatur konfigurasi R dan RStudio di `configuration.nix`.

{{< file "/etc/nixos/configuration.nix" >}}

```nix
environment = {
  systemPackages = with pkgs; let
    list-packages = with rPackages; [ # variabel pustaka R
      data_table
      dplyr
      ggplot2
      ggraph
      knitr
      leaflet
      plotly
      plyr
      rbokeh
      rmarkdown
      tidyverse
      # dan pustaka R lainnya
    ];
    r-with-packages =
    (
      rWrapper.override { # paket R
        packages = list-packages;
      }
    );
    rstudio-with-packages =
    (
      rstudioWrapper.override { # paket RStudio
        packages = list-packages;
      }
    );
  in [
    r-with-packages
    rstudio-with-packages
  ];
};
```

Bangun ulang sistem NixOS untuk memperbarui paket.

{{< cmd >}}
sudo nixos-rebuild switch
{{< /cmd >}}

Konfigurasi diatas dapat dilihat di repositori
[dotfiles](https://github.com/hervyqa/dotfire/blob/main/packages/rstudio/default.nix)
penulis.

#### Menjalankan R di Terminal

Anda dapat membuka R di terminal yang terpasang. Seperti kitty, alacritty,
konsole, urxvt, st, foot dan sebagainya.

{{< cmd >}}
R
{{< /cmd >}}

```R
R version 4.2.3 (2023-03-15) -- "Shortstop Beagle"
Copyright (C) 2023 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

[Previously saved workspace restored]

> print("Mencoba R")
[1] "Mencoba R"
>

```

{{< lineads_2 >}}

#### Menjalankan RStudio

Membuka RStudio seperti menjalankan aplikasi pada umumnya dengan membuka menu
launcher yang tersedia. Akan lebih mudah jika menggunakan DE seperti KDE atau
GNOME.

### Penutup

Satu hal yag perlu diingat, semakin banyak pustaka yang dipasang maka semakin
lama waktu startup yang dibutuhkan untuk membuka RStudio. R memiliki banyak
pustaka yang tersedia, bisa di cek di [pencarian repositori NixOS](https://search.nixos.org/packages?channel=23.05&from=0&size=50&buckets=%7B%22package_attr_set%22%3A%5B%22rPackages%22%5D%2C%22package_license_set%22%3A%5B%5D%2C%22package_maintainers_set%22%3A%5B%5D%2C%22package_platforms%22%3A%5B%5D%7D&sort=relevance&type=packages&query=r)
ada sekitar 10.000 lebih jenis pustaka. Pustaka lainnya dapat dicari di
[search.r-project](https://search.r-project.org) atau di
[METACRAN](https://www.r-pkg.org) dengan paket aktif yang mencapai 19.000 lebih.

Kedua, dengan menggunakan konfigurasi nix maka mempermudah untuk membangun
ulang untuk memperbarui konfigurasi dan versi paket. Lebih ringkas dengan
perintah `nix-rebuild switch`.

---

Demikian catatan tentang pemasangan R dan RStudio di NixOS.
Jika Anda mempunyai pertanyaan, saran, dan kritikan silahkan komentar dibawah
ini atau bisa menyapa penulis via [telegram @hervyqa](https://t.me/hervyqa).
Sekian, semoga tulisan ini bermanfaat untuk pembaca semuanya. Aamiin.
