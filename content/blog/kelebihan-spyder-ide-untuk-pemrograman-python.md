---
layout: blog
draft: false
date: 2023-04-13T11:07:11+07:00
title: "Kelebihan Spyder IDE Untuk Pemrograman Python"
description: "Alasan memilih Spyder IDE sebagai kebutuhan data science menggunakan bahasa python."
#image: "images/blog/kelebihan-spyder-ide-untuk-pemrograman-python.png"
image: images/blog/kelebihan-spyder-ide-untuk-pemrograman-python.png
type: "featured" # featured/regular
categories: # max 2
- python
- editor
---

IDE (_Integrated Development Environment_) merupakan suatu perangkat lunak yang sudah dibundel dengan perkakas yang dibutuhkan oleh seorang programmer. Umumnya terdapat linter untuk pemeriksa syntax koding, server bahasa pemrograman, autokomplit, dokumentasi, debugging dan perkakas pendukung lainnya.

Banyak jenis IDE yang bisa digunakan baik untuk awam maupun profesional, salah satunya terdapat yang dirancang untuk ilmuan, insinyur data dan data analis.

#### Spyder IDE

{{< image src="images/blog/spyder-ide.png" alt="Spyder IDE for python" >}}

[Spyder](https://www.spyder-ide.org/) merupakan perangkat lunak IDE open source cross-platform yang tersedia untuk Windows, Linux dan MacOS. Spyder dibuat secara native menggunakan bahasa Python dan Qt5. Juga memiliki fitur yang melimpah untuk kebutuhan ilmuan data, seperti eksplorasi data, analisis dan visualisasi. Sesuai dengan akronimnya SPYDER (_**S**cientific **PY**thon **D**evelopment **E**nvi**R**onment_) yang disarankan sebagai editor komputasi ilmiah.

Dalam sudut pandang sebagai pengguna free software, Spyder cocok digunakan untuk data science. Karena native dengan python dan bahasa pemrograman python tersebut juga sangat kompatibel dengan kebutuhan dibidang data science. Terlebih lagi Spyder merupakan proyek terbuka sehingga siapapun bisa berkontribusi terhadap proyek Spyder.

Kedua, builtin plugin. Sekali memasang Spyder tidak perlu lagi memasang plugin-plugin yang dibutuhkan untuk keperluan data science. Kecuali ada pustaka python yang diunduh secara terpisah.

{{< lineads_1 >}}

#### Pemasangan

Spyder IDE tidak seperti aplikasi kebanyakan yang tersedia biner secara langsung. Melainkan dipasang sebagai paket python dengan `python-pip`.

{{< cmd >}}
pip install spyder
{{< /cmd >}}

Jika menggunakan instalasi dengan [Anaconda](https://www.anaconda.com/products/distribution) dapat dipasang dengan navigator atau dengan `conda`. Tetapi kekurangan dari Anaconda sendiri ialah ukuran berkas binarinya yang cukup besar dan performanya yang lumayan berat di hardware spesifikasi rendah (keluaran lama).

{{< cmd >}}
conda install spyder
{{< /cmd >}}

Untuk pengguna **Mac** dan **Windows** disarankan mengunduh binari dari [SourgeForge](https://sourceforge.net/projects/spyder.mirror/files/) yang ukurannya lebih ringan dan tidak bloat dengan paket Anaconda.

Jika menggunakan distribusi linux biasanya sudah tersedia di repositori, tetapi tidak semua distribusi tersedia paket spyder.

* Ubuntu/Debian
  {{< cmd >}}
  sudo apt install spyder
  {{< /cmd >}}

* Fedora
  {{< cmd >}}
  sudo dnf install spyder
  {{< /cmd >}}

* Archlinux
  {{< cmd >}}
  sudo pacman -Sy spyder
  {{< /cmd >}}

Sedangkan di [NixOS](https://nixos.org/) penulis menambahkan Spyder dengan beberapa pustaka python pendukung data analisis dan visualisasi agar saling terintegrasi.

{{< file "/etc/nixos/configuration.nix" >}}

```nix
python3.withPackages (
  ps:
    with ps; [
      spyder
      jupyterlab
      matplotlib
      numpy
      plotly
      scikit-learn
      scipy
      seaborn
      sympy
      virtualenv
    ]
)
```

#### Fitur Spyder

{{< lineads_2 >}}

##### Gratis

Sebagai proyek sumber terbuka yang berada dibawah [lisensi MIT](https://github.com/spyder-ide/website-spyder/blob/master/NOTICE.txt), Spyder IDE dapat didapatkan dan digunakan secara cuma-cuma alias tidak diharuskan untuk membayar/berlangganan. Tetapi jika Anda tidak terbiasa untuk tidak membayar, sangat bisa untuk mendonasikannya melalui [OpenCollective](https://opencollective.com/spyder/).

##### Built-in plugin

Di bagian editor sudah terdapat kode analisis, linter dan snippet bahasa pemrograman python. Sehingga memudahkan pengguna untuk membuat program. Meskipun plugin dasar, namun cukup membantu.

{{< image src="images/blog/spyder-code-analysis.png" alt="Code Analysis for Spyder IDE" >}}

##### Interaktif konsol dengan ipython

Spyder menggunakan `ipython` sebagai interpreter Python bawaan yang lebih modern. Terdapat autokomplit ketika mengetik syntax yang terdapat bahasa python. Jika mengaktifkan inline plotting, ipython dapat menampilkan plot visual secara langsung di dalam konsol.

{{< image src="images/blog/spyder-ipython.png" alt="Ipython for Spyder IDE" >}}

##### Tersedia eksplorasi variabel

Menginspeksi variabel, fungsi dan objek dengan mudah. Bahkan dapat menyunting tipe data yang termasuk tipe numeric/string/boolean, tuple/list, dataframe, dan lebih banyak lagi.

{{< image src="images/blog/spyder-variable-explorer.png" alt="Variable Explorer for Spyder IDE" >}}

##### Visualisasi plot

Render hasil plot dengan pustaka matplotlib/seaborn berjalan dengan normal di dalam panel `plot`.

{{< image src="images/blog/spyder-plot.png" alt="Plot for Spyder IDE" >}}

##### Terintegrasi dokumentasi python

Dokumentasi tersedia di dalam panel Spyder. Hal ini memudahkan untuk pengguna untuk membaca lebih detail tentang fungsi/objek yang digunakan.

{{< image src="images/blog/spyder-help.png" alt="Documentation for Spyder IDE" >}}

##### Debugging

Fitur debugging harus ada untuk program IDE. Tujuannya untuk menganalisa kode program jika tidak sesuai yang dikehendaki, misalnya terdapat kode error dan fungsi/variabel yang tidak terpakai. Mencari celah kode per-baris dengan `ipdb` dari `ipython` console. Di Spyder juga tersedia list breakpoint sehingga tahu setiap baris akan mengeksekusi dan menghasilkan keluaran seperti apa. Panduan selengkapnya ada di halaman dokumentasi [debugging](https://docs.spyder-ide.org/5/panes/debugging.html).

#### Kesimpulan

Secara pribadi Spyder IDE memiliki tampilan antarmuka yang sederhana, cocok digunakan untuk pemula yang baru mempelajari bahasa python dan data analisis. Spyder juga memiliki konsep _straight on point_, fokus ke bahasa python saja. Ya bisa jadi suatu kelebihan atau suatu kekurangan yang kurang mendukung bahasa lain seperti web framework JavaScript & HTML. Mungkin developer sengaja membuat untuk tidak mendukung bahasa selain python.

Kekurangannya mungkin Spyder masih belum ada fitur untuk kolaborasi secara realtime, meskipun sudah ada [integrasi dengan git](https://docs.spyder-ide.org/current/panes/projects.html#working-with-version-control). Tentu fitur ini akan membantu perkerjaan dengan tim.

Tidak pakai VScode? sebenarnya penulis masih pakai [VSCodium](https://vscodium.com/) (turunan vscode tanpa telemetri microsoft), tetapi jarang digunakan. Terlebih lagi perlu mengkonfigurasi untuk kebutuhan pemrograman python. Kalau [Pycharm](https://www.jetbrains.com/pycharm/) memang cukup lengkap dari segi fitur, tetapi performanya saja yang kurang ideal dalam mesin laptop yang penulis gunakan. Akhirnya menggunakan memilih Spyder IDE yang fiturnya cukup lengkap, tidak terlalu berat, native dan tentunya open source.

Sejauh ini Spyder sudah menjadi IDE untuk koding sehari-hari karena memang fokusnya di python (dan R dengan RStudio). Belum ada kendala yang signifikan kecuali di performanya yang kurang gesit (hardware issue).

{{< lineads_3 >}}
