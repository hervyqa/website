---
layout: blog
draft: false
date: 2023-05-29T14:52:06+07:00
title: "Cara Memasang Jupyter Notebook Di NixOS"
description: "Jupyter Notebook merupakan antarmuka berbasis web untuk menjalankan program python. Bagaimana cara memasangnya di NixOS? ikuti panduan di blog ini."
image: "images/blog/cara-memasang-jupyter-notebook-di-nixos.png"
type: "regular" # featured/regular
categories: # max 2
- jupyter
- nixos
---

[Jupyter Notebook](https://jupyter.org) yang sebelumnya dikenal sebagai IPython
Notebook merupakan antarmuka berbasis web dari python untuk menjalankan
serangkaian perintah pertahap.
Tujuannya untuk memudahkan pengguna untuk melakukan pembelajaran, penelitian,
klarifikasi, dan visualisasi.

Jupyter Notebook diakses melalui web browser atau editor yang berbasis web
seperti vscode atau vscodium.
Umumnya tidak hanya jupyter-notebook saja yang perlu dipasang, tetapi pustaka
pendukung lainnya untuk perhitungan komputasi dan visualisasi juga perlu
dipasang.
Misalnya pustaka numpy, scipy, pandas, scikit-learn, matplotlib atau seaborn
untuk visualisasinya.

Faktanya, Jupyter Notebook juga mendukung banyak bahasa pemrograman lain seperti
Julia, Python, Haskell, Ruby, dan R.
Tetapi pada artikel ini Jupyter notebook yang dibahas adalah untuk bahasa
pemrograman python.

{{< image src="images/blog/jupyter-example.png" alt="jupyter notebook example" >}}

_Gambar 1: Contoh tampilan Jupyter Notebook._

#### Jenis pemasangan

Terlebih penulis masih menggunakan modul native dari NixOS tanpa home-manager dan
Flake, jadi pemasangan jupyter-notebook secara global di sistem NixOS dengan
`/etc/nixos/configuration.nix` atau anda bisa memecahnya menjadi beberapa berkas
konfigurasi `nix`.

Ada 2 cara untuk memasang Jupyter notebook secara global, yaitu:

1. Pemasangan dari pustaka python.
1. Pemasangan dari modul layanan systemd.

Cara yang pertama menjalankan manual layanan jupyter-notebook, menunggu beberapa
saat untuk proses jupyter selesai kemudian baru bisa digunakan secara lokal
via web browser. Modul tersebut dibundel dengan pustaka python lainnya.

Sedangkan cara yang kedua menjalankan jupyter-notebook secara otomatis saat
komputer dinyalakan, sehingga langsung membuka port via web browser.

Cara pertama atau kedua tidak masalah, terlebih lagi modul tersebut tidak
terduplikasi karena sistem di NixOS yang menggunakan symlink untuk integrasi
antar paket.
Bahkan penulis pun menggunakan kedua cara tersebut untuk mengetahui perbedaannya
dan mempelajari deklarasi jupyter di Nix.

#### Pemasangan 1: Melalui pustaka dari python

Di dalam bagian `python3.withPackages` dan seterusnya, tambahkan ke dalam
`environment.systemPackages` bersama nama paket sistem lainnya.

{{< file "/etc/nixos/configuration.nix" >}}

```nix
  environment = {
    systemPackages = with pkgs; [
      (python3.withPackages ( # tambahkan di awal baris ini.
        ps:
          with ps; [
            jupyter
            jupyterlab
            matplotlib
            numpy
            pandas
            plotly
            seaborn
            scikit-learn
            scipy
            sympy
          ]
        )
      ) # sampai baris ini.
    ];
  };
```

Anda juga bisa menambahkan pustaka python lainnya yang dibutuhkan di repositori
NixOS.

Konfigurasi ini dapat dilihat di repositori penulis
[(dotfiles cara 1).](https://github.com/hervyqa/dotfire/blob/main/packages/python/default.nix)

{{< lineads_1 >}}

#### Pemasangan 2: Melalui modul layanan systemd

Cara kedua ini menjalankan otomatis layanan jupyter sehingga pengguna dapat
langsung membuka via web browser. Tambahkan konfigurasi layanan jupyter.

{{< file "/etc/nixos/configuration.nix" >}}

```nix
  services = {
    jupyter = {
      enable = true;
      group = "jupyter";
      user = "jupyter";
      password = "'argon2:$argon2id$v=19$m=10240,t=10,p=xxxxxxx... '"; # dienkripsi
      kernels = {
        python3 = let
          env = (pkgs.python3.withPackages (ps: with ps; [
                jupyterlab
                matplotlib
                numpy
                pandas
                plotly
                seaborn
                selenium
                scikit-learn
                scipy
                sympy
                statsmodels
              ]
          ));
        in {
          displayName = "Python for Machine Learning";
          argv = [
            "${env.interpreter}"
            "-m"
            "ipykernel_launcher"
            "-f"
            "{connection_file}"
          ];
          language = "python";
        };
      };
    };
```

* `group` dan `user` sebaiknya tetap menggunakan `jupyter`.
* `password` dapat diperoleh dengan menjalankan perintah dibawah ini di terminal.

  ```sh
  python -c 'from notebook.auth import passwd; print(passwd())'
  ```

* Ganti password sesuai yang dikehendaki, salin dan tempelkan ke `password =`.

Konfigurasi diatas dapat dilihat di repositori
[(dotfiles: cara 2)](https://github.com/hervyqa/dotfire/blob/main/services/jupyter/default.nix)

Selanjutnya beri hak akses user dan grup jupyter. Tambahkan `jupyter` ke dalam
`extraGroups` milik pengguna.

{{< file "/etc/nixos/configuration.nix" >}}

```nix
  users = {
    users = {
      # tambahkan grup jupyter
      jupyter = {
        group = "jupyter";
      };
      hervyqa = { # contoh username
        isNormalUser = true;
        description = "Hervy Qurrotul"; # contoh fullname
        extraGroups = [
          "jupyter" # tambahkan pengguna ke grup jupyter
          "audio"
          "disk"
          "input"
          "users"
          "video"
          "wheel"
          # ... dan grup opsional lainnya
        ];
      };
    };
  };
```

Konfigurasi di atas dapat dilihat di repositori
[(dotfiles: cara 2 konfigurasi grup).](https://github.com/hervyqa/dotfire/blob/main/system/users/default.nix)

Perbedaan dari cara sebelumnya:

* Layanan `jupyter.service` akan berjalan otomatis saat komputer dinyalakan.
* Dikarenakan menggunakan user `jupyter` dan grup `jupyter`, maka direktori
proyek jupyter yang dibuat akan diletakkan di `/var/lib/jupyter` dengan hak
akses `jupyter:jupyter`.
* Hanya user `jupyter` saja yang bisa mengakses melalui web browser dengan
kata sandi.

{{< lineads_2 >}}

#### Ekstensi Jupyter Notebook VSCode

Sebagai pendukung di IDE editor (opsional), tambahkan beberapa ekstensi
Jupyter Notebook di vscode atau vscodium.

```nix
  environment = {
    systemPackages = with pkgs; [
      (
        vscode-with-extensions.override {
          vscode = vscodium; # jika menggunakan vscodium
          vscodeExtensions = with vscode-extensions;
            [
              ms-pyright.pyright
              ms-python.python
              ms-toolsai.jupyter
              ms-toolsai.jupyter-keymap
              ms-toolsai.jupyter-renderers
              ms-toolsai.vscode-jupyter-cell-tags
              ms-toolsai.vscode-jupyter-slideshow
            ]
        }
      )
    ];
  };
```

#### Menjalankan Notebook melalui web browser

Setelah selesai mengkonfigurasi jupyter-notebook. Bangun ulang sistem NixOS.

{{< cmd >}}
sudo nixos-rebuild switch
{{< /cmd >}}

Kemudian pengguna dapat membukanya melalui web browser.

* Buka tautan [http://localhost:8888](http://localhost:8888).
* Login dan masuk menggunakan password jika ada.

{{< image src="images/blog/jupyter-login.png" alt="jupyter notebook login" >}}

_Gambar 2: Halaman login Jupyter Notebook._

#### Penutup

NixOS menggunakan deklarasi untuk konfigurasi sistem operasinya baik dari segi
sistem root maupun user.
Terlihat menyakitkan karena butuh konfigurasi yang cukup panjang, tetapi
konfigurasi tersebut diperlukan sekali dan hasilnya dapat digunakan
berulang-ulang di mesin yang berbeda.

**Jupyter Notebook vs IDE lebih baik yang mana?**

Bagi penulis keduanya terbaik, tergantung kebutuhan. Jupyter lebih disarankan
untuk kebutuhan yang cepat dan sederhana.
Sedangkan jika menggunakan IDE misalnya seperti Spyder IDE, VSCode, PyCharm
tentu lebih memiliki keunggulan fitur spesifik.
Kembali lagi kepada kebutuhan pengguna.

Demikian catatan dari penulis.
Jika Anda mempunyai pertanyaan, saran, dan kritikan silahkan komentar dibawah
ini atau bisa menyapa penulis via [telegram @hervyqa](https://t.me/hervyqa).
Sekian, semoga tulisan ini bermanfaat untuk pembaca semuanya. Aamiin.

{{< lineads_3 >}}
