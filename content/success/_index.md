---
title: "Berhasil terkirim!"
description: "Formulir berhasil terkirim"
image: "images/page/bg-kontak.png"
layout: "success"
draft: false
---
Anda sudah mengisi formulir, saya akan segera merespon pada jam kerja (08.00-17.00 GMT+7).

_Syukran wa jazaakumullahu khairan katsiran._

